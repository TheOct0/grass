'use strict';

const Sequelize = require('sequelize');
const db = new Sequelize({
  dialect: 'sqlite',
  storage: './db/devices.sqlite'
});
const Device = db.define("device", {
  token: Sequelize.STRING,
  name: Sequelize.STRING,
  mac: Sequelize.STRING,
  ip: Sequelize.STRING,
  nickname: Sequelize.STRING
}, {
  timestamps: false,
  freezeTableName: true
});
const Local = db.define("local", {
  token: Sequelize.STRING,
  name: Sequelize.STRING,
  nickname: Sequelize.STRING
}, {
  timestamps: false,
  freezeTableName: true
});
Device.sync();
Local.sync();

module.exports = {
  db, Device, Local
};

const script = require("./script.js");
