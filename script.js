'use strict';

var main = require("./index.js");

var dgram = require('dgram');
var udp = dgram.createSocket('udp4');

var os = require("os");
var ifaces = os.networkInterfaces();

var net = require('net');
var JsonSocket = require('json-socket');
var tcpServer = net.createServer();

var port = 7357;
var platform = process.platform;
var name = os.hostname();
var localNetInterfaces = ip_getInterfaces();
var localIP = ip_getLocal();
var localMAC = mac_getLocal();
var netMask = ip_getNetMask();
var broadcast = ip_calculateBroadcast();
var localToken = "";

module.exports = {
  port, platform, localNetInterfaces, localIP, localMAC, netMask, broadcast
};

udp.bind(port, function() {
  udp.setBroadcast(true);
});

tcpServer.listen(port);

tcpServer.on("connection", function (socket) {
  socket = new JsonSocket(socket);
  socket.on("message", function (message) {
    if (message.type == "info_req") {
      let mac = localMAC[localIP.indexOf(message.ip)];
      socket.sendEndMessage({
        name: name,
        mac: mac
      });
    }
  });
});

udp.on("message", (msg, rinfo) => {
  msg = msg.toString();
  if (msg.startsWith("$TKN_")) {
    let token = msg.split("_")[1];
    main.Device.findAll({ where: { token: token, ip: rinfo.address } })
      .then(res => {
        if (res.length == 0) {
          main.Device.create({
            token: token,
            ip: rinfo.address
          }).then(function () {
            let socket = new JsonSocket(new net.Socket());
            socket.connect(port, rinfo.address);
            socket.on("connect", function () {
              socket.sendMessage({
                type: "info_req",
                ip: rinfo.address
              });
              socket.on("message", function (message) {
                console.log(message);
                main.Device.update({
                  token: token,
                  ip: rinfo.address,
                  name: message.name,
                  mac: message.mac
                }, { where: {
                  token: token,
                  ip: rinfo.address
                } });
              });
            });
          });
        }
      });
  }
});

function init() {
  setInterval(function() {
    deviceDiscovery();
  }, 1000);
}

function tokenGen() {
  let text = "";
  let chrs = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  for (let i = 0; i < 16; i++) {
    text += chrs.charAt(Math.floor(Math.random() * chrs.length));
  }
  return text;
}

main.Local.findOne({}).then(res => {
  if (res === null) {
    localToken = tokenGen();
    main.Local.create({
      token: localToken,
      name: name
    }).then(init);
  } else {
    localToken = res.token;
    init();
  }
});

function deviceDiscovery() {
  for (var i = 0; i < broadcast.length; i++) {
    let msg = "$TKN_" + localToken;
    udp.send(msg, 0, msg.length + 5, port, broadcast[i]);
  }
}

function tcpNewDevice(ip) {
  let tcpSocket = new JsonSocket(new net.Socket());
  tcpSocket.connect(port, ip, function (err) {
    if (err) throw err;
    let info = {
      type: "info",
      platform: platform,
      name: name
    }
    tcpSocket.sendMessage(info);
    tcpSocket.end();
  })
}

function ip_getInterfaces() {
  var res = [];
  Object.keys(ifaces).forEach(function (ifname) {
    ifaces[ifname].forEach(function (iface) {
      if ('IPv4' === iface.family && iface.internal === false) {
        res.push(ifname);
      }
    });
  });
  return res;
}

function ip_getNetMask() {
  var res = [];
  Object.keys(ifaces).forEach(function (ifname) {
    ifaces[ifname].forEach(function (iface) {
      if ('IPv4' === iface.family && iface.internal === false) {
        res.push(iface.netmask);
      }
    });
  });
  return res;
}

function ip_getLocal() {
  var res = [];
  Object.keys(ifaces).forEach(function (ifname) {
    ifaces[ifname].forEach(function (iface) {
      if ('IPv4' === iface.family && iface.internal === false) {
        res.push(iface.address);
      }
    });
  });
  return res;
}

function mac_getLocal() {
  var res = [];
  Object.keys(ifaces).forEach(function (ifname) {
    ifaces[ifname].forEach(function (iface) {
      if ('IPv4' === iface.family && iface.internal === false) {
        res.push(iface.mac);
      }
    });
  });
  return res;
}

function ip_calculateBroadcast(ipaddr, mask) {
  var res = [];
  for (var i = 0; i < localIP.length; i++) {
    var binIPAddr = "";
    var binMask = "";
    var netBits = "";
    binIPAddr = ip_toBinary(localIP[i]);
    binMask = ip_toBinary(netMask[i]);
    netBits = Number(binMask.indexOf("0"));
    res.push(ip_toDecimal(padright(binIPAddr.substr(0, netBits), 32, "1")));
  }
  return res;
}

function padright(number, length, ch) {
  var str = '' + number;
  while (str.length < length) {
    str = str + ch;
  }
  return str;
}

function padleft(number, length, ch) {
  var str = '' + number;
  while (str.length < length) {
    str = ch + str;
  }
  return str;
}

function ip_toBinary(decimal) {
  var binOct = "";
  var bin = "";
  var octets = decimal.split(".");
  for (var i=0; i < octets.length; i++){
    binOct = Number(octets[i]).toString(2);
    binOct = padleft(binOct,8,"0");
    bin = bin + binOct;
  }
  return bin;
}

function ip_toDecimal(binary) {
  var decimal="";
  var octet1 = parseInt(binary.substr(0,8),2);
  var octet2 = parseInt(binary.substr(8,8),2);
  var octet3 = parseInt(binary.substr(16,8),2);
  var octet4 = parseInt(binary.substr(24,8),2);
  decimal = octet1 + "." + octet2 + "." + octet3 + "." + octet4;
  return decimal;
}
